import { Button, Row, Col} from 'react-bootstrap';
import Banner from '../components/Banner'

// Solution no.1
/*
export default function Banner() {

	return(
		<Row>
			<Col>
				<h1>Error 404 - Page Not Found!</h1>
				<p>The page you are looking can not be found here.</p>
				<Button variant="primary"> Back to Home </Button>
			</Col>
		</Row>
	)
};

*/

// solution no.2 

export default function Error() {

	const data ={
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking can not be found",
		destination: "/",
		label: "Back to Home"

	}

	return(
		<Banner data={data} />
	)
}













