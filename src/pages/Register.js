import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
	
	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ firstName, setfirstName ] = useState('');
	const [ lastName, setlastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ Mnumber, setMnumber ] = useState('');
	const [ Password1, setPassword1 ] = useState('');
	const [ Password2, setPassword2 ] = useState('');
	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive] = useState(false);

	useEffect(() => {


		// Validation to enable submit buttion when all fields are populated and both paswords match.
		if (firstName !== '' && lastName !== '' && email !== '' && Mnumber !== '' && Mnumber.length === 10 && Password1 !== '' && Password2 !== '' && (Password2 === Password1) ) {
			setIsActive(true);
		} else {

			setIsActive(false);
		}
	})

	// function registerUser(e) {
	// 	e.preventDefault();

	// 	// MiniActivity
	// 	// Write a code that empty the input field for email, password1, password2

	// 	setPassword1('');
	// 	setPassword2('');
	// 	setEmail('');
	// 	alert("Thank you for registered!");
	// }

	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({

				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				Swal.fire({
					title: 'Duplicate email found!',
					icon: 'error',
					text: 'Please provide different email'
				})

			} else {

				// fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				fetch('http://localhost:4000/users/register', {

					method: "POST",
					headers: {

						'Content-Type': 'application/json'
					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: Password1
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true) {

						setfirstName('');
						setlastName('');
						setEmail('');
						setPassword1('');
						setPassword2('');

						Swal.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Welcome to Zuitt!'
						});

						navigate('/home');
					} else {

						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						});
					};
				})
	
			}

		});
	}

	// function checkMail (e) {

	// 	fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	// 		method: 'POST',
	// 		headers: {
	// 			'Content-Type': 'application/json'
	// 		},
	// 		body: JSON.stringify({
	// 			email: email			
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(result => {console.log(result);  return result});

	// }

	return (

		// (checkMail() === false)?
		(user.id !== null)? 
			<Navigate to="/login" />
		:		
			<Form onSubmit={(e) => {registerUser(e)}}>
			{/*<Form onSubmit={(e) => registerUser(e)}>*/}
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e => setfirstName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e => setlastName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group controlId="firstName">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter Valid Mobile Number"
						value={Mnumber}
						onChange={e => setMnumber(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={Password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={Password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?
					<Button variant="primary" type='submit' id=' submitBtn'> 
						Submit
					</Button>
					:
					<Button variant="danger" type='submit' id=' submitBtn' disabled> 
						Submit
					</Button>
				}

			</Form>
	)
}
