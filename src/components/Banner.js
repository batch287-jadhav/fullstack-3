import { Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

// data is for error page
export default function Banner({data}) {

	const { title, content, destination, label } = data;

	return(
		<Row>
			<Col>
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination} > {label}</Button>
			</Col>
		</Row>
	)
};

























