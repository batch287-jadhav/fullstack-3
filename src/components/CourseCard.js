import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'

export default function CourseCard({course}) {
	console.log(course)

const { name, description, price, _id } = course;

// console.log(course.name);
// console.log(course.price);
// console.log(course.description);

// React Hook that lets you add a state variable to the components.
// Use the state hook for this component to be able to store its state
// States are used to keep track of information related to individual components

// Syntax:
	// const [ getter, setter ] = useState(initialGetterValue)

// State Hook
// const [ count, setCount ] = useState(0);
// const [ seats, setSeats ] = useState(30);
// const [ isOpen, setIsOpen ] = useState(true);

// function enroll() {
// 	if(seats > 0){
// 		setCount(count + 1);
// 		console.log('Enrollees: ' + count)
// 		setSeats(seats - 1);
// 		console.log('Seats: ' + seats)
// 	} 
// };
	
	// useEffect - allows us to instruct the app that the component needs to do something
	// Keyword for userEffect, "reactive". It makes our app, reactive.

	// useEffect(() => {
	// 	if(seats === 0 && count === 30){
	// 		setIsOpen(false);
	// 		alert("No More Seats!");
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// }, [ count, seats ])

	return (
		<Row className="mt-3 mb-3">
			<Col xs={12}>
				<Card className="cardHighlight p-0">
					<Card.Body>
						<Card.Title><h4>{name}</h4></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}





































